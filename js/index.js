const url = 'https://lobinhos.herokuapp.com/wolves/';
const $ = document;
function adicionaFotoLobinho(links) {
	let foto1 = $.getElementById('foto-1');
	let foto2 = $.getElementById('foto-2');
	foto1.src = links[0];
	foto2.src = links[1];
}
function adicionaNomeLobo(lista_nome) {
	let nome1 = $.getElementById('nome-1');
	let nome2 = $.getElementById('nome-2');
	console.log(lista_nome[0]);
	nome1.innerHTML = lista_nome[0];
	nome2.innerHTML = lista_nome[1];
}
function adicionarIdadeLobo(lista_idade) {
	let idade1 = $.getElementById('idade-1');
	let idade2 = $.getElementById('idade-2');
	idade1.innerHTML = 'Idade: ' + lista_idade[0] + ' anos';
	idade2.innerHTML = 'Idade: ' + lista_idade[1] + ' anos';
}
function adicionaDescricao(lista_descricao) {
	let descricao1 = $.getElementById('descricao-1');
	let descricao2 = $.getElementById('descricao-2');
	descricao1.innerHTML = lista_descricao[0];
	descricao2.innerHTML = lista_descricao[1];
}
fetch(url)
	.then((data) => data.json())
	.then((resp) => {
		let links_fotos = [];
		let lista_nome = [];
		let lista_idade = [];
		let lista_descricao = [];
		for (let i = 0; i < 2; i++) {
			links_fotos.push(resp[i].link_image);
			lista_nome.push(resp[i].name);
			lista_idade.push(resp[i].age);
			lista_descricao.push(resp[i].description);
		}

		adicionaNomeLobo(lista_nome);
		adicionarIdadeLobo(lista_idade);
		adicionaDescricao(lista_descricao);
		adicionaFotoLobinho(links_fotos);
	})
	.catch((err) => console.log(err));
